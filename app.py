#!/usr/bin/env python3

import bottle
from bottle import get, post, static_file, request, route, template
from bottle import SimpleTemplate
from configparser import ConfigParser
from ldap3 import Connection, Server
from ldap3 import SIMPLE, SUBTREE, MODIFY_REPLACE
from ldap3.core.exceptions import LDAPBindError, LDAPConstraintViolationResult, \
    LDAPInvalidCredentialsResult, LDAPUserNameIsMandatoryError, \
    LDAPSocketOpenError, LDAPExceptionError
import logging
import os
from os import environ, path
from validate_email import validate_email
import random, string

from sqlalchemy import create_engine, Column, Integer, Sequence, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from smtplib import SMTP


BASE_DIR = path.dirname(__file__)
LOG = logging.getLogger(__name__)
LOG_FORMAT = '%(asctime)s %(levelname)s: %(message)s'
VERSION = '2.1.0'

"""
SQLite session
"""

Base = declarative_base()
engine = create_engine('sqlite:///%s/sqlite3.db' % BASE_DIR, echo=True)

class Token(Base):
    __tablename__ = 'token'
    email = Column(String(250), primary_key=True)
    token = Column(String(32))

    def __init__(self, email, token):
        self.email = email
        self.token = token

Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
sqlSession = DBSession()


"""
Routes
"""

@get('/')
def get_index():
    return index_tpl()

@get('/change-password')
def get_changePassword():
    return changePassword_tpl()

@post('/change-password')
def post_changePassword():
    form = request.forms.getunicode

    def error(msg):
        return index_tpl(username=form('username'), alerts=[('error', msg)])

    if form('new-password') != form('confirm-password'):
        return error("Password doesn't match the confirmation!")

    if len(form('new-password')) < 8:
        return error("Password must be at least 8 characters long!")

    try:
        change_password(form('username'), form('old-password'), form('new-password'))
    except Error as e:
        LOG.warning("Unsuccessful attempt to change password for %s: %s" % (form('username'), e))
        return error(str(e))

    LOG.info("Password successfully changed for: %s" % form('username'))

    return index_tpl(alerts=[('success', "Password has been changed")])


@get('/recover-password')
def get_recoverPassword():
    return recoverPassword_tpl()

@post('/recover-password')
def post_recoverPassword():
    form = request.forms.getunicode
    
    def error(msg):
        return recoverPassword_tpl(email=form('email'), alerts=[('error', msg)])
    
    if not validate_email(form('email')):
        return error("Not a valid email address")

    try:
        token = create_recover_passwd_token(form('email'))
        if token:
            if sendEmail(form('email'), token):
                return index_tpl(alerts=[('success', "We sent you an email!")])
            
    except Error as e:
        LOG.warning("Unsuccessful attempt to create sqlite token for %s" % (form('email'), e))
        return error(str(e))

    return recoverPassword_tpl()


@get('/reset-password/:token_string')
def resetPassword(token_string):

    def error(msg):
        return index_tpl(alerts=[('error', msg)])

    token = sqlSession.query(Token).filter_by(token=token_string).first()
    if not token:
        return error("Token not found")

    return resetPassword_tpl()


@post('/reset-password/:token_string')
def resetPassword(token_string):
    form = request.forms.getunicode
    
    def error(msg):
        return resetPassword_tpl(alerts=[('error', msg)])

    if form('new-password') != form('confirm-password'):
        return error("Password doesn't match the confirmation!")

    if len(form('new-password')) < 8:
        return error("Password must be at least 8 characters long!")

    try:
        token = sqlSession.query(Token).filter_by(token=token_string).first()
        if not token:
            return error("Token not found")
        
        with connect_ldap(CONF['ldap']) as c:
            user_dn = find_user_by_mail(CONF['ldap'], c, token.email)

        # Note: raises LDAPUserNameIsMandatoryError when user_dn is None.
        with connect_ldap(CONF['ldap'], authentication=SIMPLE, user=CONF['ldap']['admin_dn'], password=CONF['ldap']['admin_passwd']) as c:
            c.bind()
            changes = {'userPassword': [(MODIFY_REPLACE, [form('new-password')])] }
            c.modify(user_dn, changes=changes)
            
            sqlSession.delete(token)
            sqlSession.commit()
            
        return index_tpl(alerts=[('success', "Password has been changed")])
            
    except LDAPExceptionError as e:
        return index_tpl(alerts=[('error', e)])

    return index_tpl(alerts=[('error', "Something went wrong")])


@route('/static/<filename>', name='static')
def serve_static(filename):
    return static_file(filename, root=path.join(BASE_DIR, 'static'))



"""
Templates
"""

def index_tpl(**kwargs):
    return template('index', **kwargs)

def changePassword_tpl(**kwargs):
    return template('changePassword', **kwargs)

def recoverPassword_tpl(**kwargs):
    return template('recoverPassword', **kwargs)

def resetPassword_tpl(**kwargs):
    return template('resetPassword', **kwargs)


"""
Functions
"""

def change_password(username, old_pass, new_pass):

    LOG.debug("Changing password for %s" % (username))

    try:
        with connect_ldap(CONF['ldap']) as c:
            user_dn = find_user_by_uid(CONF['ldap'], c, username)

        # Note: raises LDAPUserNameIsMandatoryError when user_dn is None.
        with connect_ldap(CONF['ldap'], authentication=SIMPLE, user=user_dn, password=old_pass) as c:
            c.bind()
            c.extend.standard.modify_password(user_dn, old_pass, new_pass)

    except (LDAPBindError, LDAPInvalidCredentialsResult, LDAPUserNameIsMandatoryError):
        raise Error('Username or password is incorrect!')

    except LDAPConstraintViolationResult as e:
        # Extract useful part of the error message (for Samba 4 / AD).
        msg = e.message.split('check_password_restrictions: ')[-1].capitalize()
        raise Error(msg)

    except LDAPSocketOpenError as e:
        LOG.error('{}: {!s}'.format(e.__class__.__name__, e))
        raise Error('Unable to connect to the remote server.')

    except LDAPExceptionError as e:
        LOG.error('{}: {!s}'.format(e.__class__.__name__, e))
        raise Error('Encountered an unexpected error while communicating with the remote server.')

def create_recover_passwd_token(email):
    LOG.debug("Create token for %s" % (email))

    try:
        with connect_ldap(CONF['ldap']) as c:
            user_dn = find_user_by_mail(CONF['ldap'], c, email)
            if not user_dn:
                return None
    except:
        raise Error('Encountered an unexpected error searching ldap for mail attribute.')
        return None
  
    token = sqlSession.query(Token).filter_by(email=email).first()
    if token:
        sqlSession.delete(token) 
    
    rand_str = lambda n: ''.join([random.choice(string.ascii_lowercase) for i in range(n)])
    token_string = rand_str(32)
    
    token = Token(email=email, token=token_string)
    sqlSession.add(token) 
    sqlSession.commit()
    return token_string




def sendEmail(email, token):
    subject = "Reset your password"
    link = "%s/reset-password/%s" % (CONF['server']['host'], token)
    
    print(link)
    return True
    
    message = """\
From: %s
To: %s
Subject: %s

Please go here:  %s
""" % (CONF['smtp']['no_reply'], ", ".join(email), subject, link)

    # Send the mail
    try:
        server = smtplib.SMTP(CONF['smtp']['host'])
        server.sendmail(FROM, TO, message)
        server.quit()
        return True
    except:
        return False


"""
LDAP
"""

def find_user_by_uid(conf, conn, uid):
    search_filter = conf['search_filter'].replace('{uid}', uid)
    conn.search(conf['base'], "(%s)" % search_filter, SUBTREE)

    return conn.response[0]['dn'] if conn.response else None


def find_user_by_mail(conf, conn, email):
    conn.search(conf['base'], "(&(objectclass=inetOrgPerson)(mail=%s))" % email, SUBTREE)

    return conn.response[0]['dn'] if conn.response else None

def connect_ldap(conf, **kwargs):
    server = Server(host=conf['host'],
                    port=conf.getint('port', None),
                    use_ssl=conf.getboolean('use_ssl', False),
                    connect_timeout=5)

    return Connection(server, raise_exceptions=True, **kwargs)

"""
Bottle
"""

def read_config():
    config = ConfigParser()
    config.read([path.join(BASE_DIR, 'settings.ini'), os.getenv('CONF_FILE', '')])

    return config


class Error(Exception):
    pass


if environ.get('DEBUG'):
    bottle.debug(True)

# Set up logging.
logging.basicConfig(format=LOG_FORMAT)
LOG.setLevel(logging.INFO)
LOG.info("Starting ldap-passwd-webui %s" % VERSION)

CONF = read_config()

bottle.TEMPLATE_PATH = [BASE_DIR]

# Set default attributes to pass into templates.
SimpleTemplate.defaults = dict(CONF['html'])
SimpleTemplate.defaults['url'] = bottle.url


# Run bottle internal server when invoked directly (mainly for development).
if __name__ == '__main__':
    bottle.run(**CONF['server'])
# Run bottle in application mode (in production under uWSGI server).
else:
    application = bottle.default_app()
